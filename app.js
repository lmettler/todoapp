const fs = require('fs')
const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const PORT =  3000;


app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json())



app.get('/todos', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    fs.readFile('todos.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            res.send(JSON.stringify(jsonParsed));
        }
    );
});

app.get('/todos/:id', function (req, res) {
    res.setHeader('Content-Type', 'application/json');
    let id = req.params.id - 1;

    fs.readFile('todos.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            res.send(JSON.stringify(jsonParsed[id]));
        }
    );
});

app.post('localhost:3000/todos', function (req, res) {
    fs.readFile('todos.json',
        function (err, data) {
            let jsonParsed = JSON.parse(data);
            let jsonContent = JSON.stringify(jsonParsed);

            fs.writeFile("todos.json", jsonContent, 'utf8', function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("JSON file has been saved.");
            });
        });

    res.sendStatus(200);
});

app.listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`)
})
