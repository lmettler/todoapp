
const urlTodos = 'http://localhost:3000/todos';


function sendTodo (){
    let taskInput = document.getElementById('taskinput').value;
    console.log(taskInput);
    console.log(urlTodos);

    let task = {
        "task": taskInput
    };
    console.log(task);

    fetch(urlTodos,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(task)
        })
        .then(function (res) {
            console.log(res)
            window.location.href = '../index.html'
        })
        .catch(function (res) {
            console.log(res)
        });


}

function createTodo (taskInput){
    let taskDiv = document.createElement("div");
    taskDiv.className = "task";
    taskDiv.innerHTML = taskInput;
    document.getElementById("opentasks").appendChild(taskDiv);
}

fetch(urlTodos, {method: 'GET'})
    .then(res => res.json())
    .then(function (data) {

        let todo = document.getElementById("opentasks");
        todo.src = data.task;

        if (data.length === 0) {
            let pComment = document.createElement("p");
            pComment.className = "comment";
            pComment.innerHTML = "There are no open Tasks";
            document.getElementById("opentasks").appendChild(pComment);

        } else {
            for (let i = 0; i < data.length; i++) {
                let d = data[i];
                createTodo(d.task)
            }
        }

    });